const cities = [
  {
    name: "New York",
    timeZone: "America/New_York",
    imageURL: "https://images.unsplash.com/photo-NEW-URL-FOR-NEW-YORK-IMAGE",
  },
  {
    name: "London",
    timeZone: "Europe/London",
    imageURL:
      "https://images.unsplash.com/photo-1505761671935-60b3a7427bad?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
  },
  {
    name: "Beijing",
    timeZone: "Asia/Shanghai",
    imageURL:
      "https://images.unsplash.com/photo-1500021804447-2ca2eaaaabeb?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
  },
  {
    name: "Tokyo",
    timeZone: "Asia/Tokyo",
    imageURL: "https://images.unsplash.com/photo-NEW-URL-FOR-TOKYO-IMAGE",
  },
  {
    name: "Sydney",
    timeZone: "Australia/Sydney",
    imageURL: "https://images.unsplash.com/photo-NEW-URL-FOR-SYDNEY-IMAGE",
  },
];

function updateTime() {
  // Update Stockholm time
  document.getElementById("stockholm-time").textContent =
    new Date().toLocaleTimeString("en-GB", { timeZone: "Europe/Stockholm" });

  // Update other cities' times
  cities.forEach((city) => {
    const timeDisplay = document.getElementById(`${city.name}-time`);
    if (timeDisplay) {
      timeDisplay.textContent = new Date().toLocaleTimeString("en-GB", {
        timeZone: city.timeZone,
      });
    }
  });
}

// Update the time immediately and then every second
updateTime();
setInterval(updateTime, 1000);

// Add other cities to the DOM
const otherCitiesContainer = document.getElementById("other-cities");
cities.forEach((city) => {
  const cityDiv = document.createElement("div");
  cityDiv.id = city.name;
  cityDiv.className = "time-display";
  cityDiv.innerHTML = `
    <h2 class="text-xl text-center text-blue-500">${city.name}</h2>
    <img src="${city.imageURL}" class="w-full h-64 object-cover" />
    <p id="${city.name}-time" class="text-center text-xl"></p>
  `;
  otherCitiesContainer.appendChild(cityDiv);
});
